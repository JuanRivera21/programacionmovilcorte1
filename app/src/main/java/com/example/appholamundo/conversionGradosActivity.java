package com.example.appholamundo;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class conversionGradosActivity extends AppCompatActivity {

    private EditText txtCantidad;
    private Button btnCalcular, btnLimpiar, btnCerrar;

    private RadioButton rdbCel, rdbFar;

    private TextView txtResultado;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_conversion_grados);

        iniciarComponentes();
        //Evento click calcular

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                float grados = 0.0f;
                if(txtCantidad.getText().toString().matches("")){
                    Toast.makeText(getApplicationContext(),
                            "Capturar la Cantidad",Toast.LENGTH_SHORT).show();
                }
                else{
                    grados = Float.parseFloat(txtCantidad.getText().toString());
                    if(rdbCel.isChecked()){
                        //Convertir de cel a fahr
                        grados=(grados*9/5)+32;
                        txtResultado.setText("Resultado: "+ grados + " F");
                    }else {
                        //Convertir de fahr a cel
                        grados=(grados-32)*5/9;
                        txtResultado.setText("Resultado: "+ grados + " C");
                    }
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txtResultado.setText("Resultado: ");
                txtCantidad.setText("");
                rdbCel.setChecked(true);
                rdbFar.setChecked(false);
            }
        });

        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void iniciarComponentes(){
        txtCantidad = (EditText) findViewById(R.id.txtCantidad);
        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnCerrar = (Button) findViewById(R.id.btnCerrar);
        rdbCel = (RadioButton) findViewById(R.id.rdbCel);
        rdbFar = (RadioButton) findViewById(R.id.rdbFar);
        txtResultado = (TextView) findViewById(R.id.txtResultado);
    }

}