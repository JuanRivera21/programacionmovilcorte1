package com.example.appholamundo;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private TextView lblSaludo;
    private EditText txtSaludo;
    private Button btnSaludo, btnCerrar, btnLimpiar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);
//Relacionar los objetos con los componentes del layout
        lblSaludo = findViewById(R.id.lblSaludo);
        txtSaludo = findViewById(R.id.txtNombre);
        btnSaludo = findViewById(R.id.btnPulsame);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCerrar = findViewById(R.id.btnCerrar);
//Codificar eventor de click a boton limpiar
        btnLimpiar.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                txtSaludo.setText("");
                lblSaludo.setText("");
            }
        });

//Codificar evento de click de boton cerrar
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



        btnSaludo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nombre = txtSaludo.getText().toString();

                if (!nombre.isEmpty()) {
                    Toast.makeText(MainActivity.this, "¡Hola, " + nombre + "!", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Por favor, ingresa tu nombre.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}